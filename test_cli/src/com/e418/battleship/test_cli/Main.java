package com.e418.battleship.test_cli;

import com.e418.battleship.net.AuthClient;
import com.e418.battleship.server.ServerMain;
import com.e418.battleship.server.packetlib.PacketUtils;
import com.e418.battleship.server.packetlib.client.AuthRequest;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.Arrays;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    private static final String AUTH_BASE = "http://127.0.0.1:9639";
    private static final String GAME_SERVER_IP = "127.0.0.1";
    private static final int GAME_SERVER_PORT = ServerMain.PORT;
    private static String username;
    private static InputStream stream1 = null;
    private static ChatThread chatThread = null;
    private static String chatStr = "";

    static void prompt() {
        System.out.println("> ");
        //System.out.flush();
    }

    private static String doLogin(Scanner input) {
        System.out.println("Please enter your username:");
        prompt();
        username = input.next();

        System.out.println("Please enter your password:");
        prompt();
        String password = input.next();

        AuthClient client = new AuthClient(username, password, AUTH_BASE);
        String token = client.doLogin();

        if (token == null) {
            System.out.println("ERROR: Login didn't work, please try again...");
            return null;
        }

        // We're good!
        System.out.println("Login successful!");
        return token;
    }

    private static void doRegister(Scanner input) {
        System.out.println("Please enter a new username:");
        prompt();
        username = input.next();

        System.out.println("Please enter a new password:");
        prompt();
        String password = input.next();

        AuthClient client = new AuthClient(username, password, AUTH_BASE);
        boolean result = client.doRegister();

        if (!result) {
            System.out.println("ERROR: Username already in use!");
            return;
        }

        // We're good!
        System.out.println("Registration successful!");
    }

    private static void start() {
        Scanner input = new Scanner(stream1);
        String authToken = null;

        while (authToken == null) {
            System.out.println("== Welcome to the Battleship Royale CLI ==");
            System.out.println("What do you want to do?\n1: Login\n2: Register");
            prompt();

            // Should we login or register
            int loginOrRegister = input.nextInt();

            switch (loginOrRegister) {
                case 1:
                    System.out.println("== LOGIN ==");
                    authToken = doLogin(input);
                    break;
                case 2:
                    System.out.println("== REGISTER ==");
                    doRegister(input);
                    break;
                default:
                    break;
            }
        }

        // Show the main menu
        MainMenu mainMenu = new MainMenu(input, username, authToken, chatThread, GAME_SERVER_IP, GAME_SERVER_PORT);
        mainMenu.showMainMenu();
    }

    private static InputStream createStream(final LinkedBlockingQueue<Character> sb) {
        return new InputStream() {
            private boolean taken = false;
            @Override
            public int read() throws IOException {
                //System.out.println("Here 1");
                int c;

                if (taken) {
                    this.taken = false;
                    return -1;
                }

                try {
                    c = sb.take();
                    taken = true;
                    return c;
                } catch(InterruptedException ie) {
                    throw new IOException();
                }
            }
        };
    }

    private static void createGUI() {
        final int WIDTH = 1000;
        final int HEIGHT = 1000;

        // Our queues
        final LinkedBlockingQueue<Character> sb1 = new LinkedBlockingQueue<>();

        // Create our new window frame
        JFrame frame = new JFrame();

        JTextArea gameArea = new JTextArea();
        JTextArea chatArea = new JTextArea();
        gameArea.setEditable(false);
        chatArea.setEditable(false);

        // Create our fake streams
        stream1 = createStream(sb1);

        final JTextField t1 = new JTextField();
        t1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                sb1.offer(e.getKeyChar());

                if (e.getKeyChar() == '\n')
                    t1.setText("");
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        final JTextField t2 = new JTextField();
        t2.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == '\n') {
                    t2.setText("");
                    chatThread.sendChatPacket(chatStr);
                    chatStr = "";
                    return;
                }

                chatStr += e.getKeyChar();
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });


        JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, gameArea, t1);
        spLeft.setDividerSize(8);
        spLeft.setResizeWeight(0.9);
        spLeft.setDividerLocation(0.9);
        spLeft.setContinuousLayout(true);

        JSplitPane spRight = new JSplitPane(JSplitPane.VERTICAL_SPLIT, chatArea, t2);
        spRight.setDividerSize(8);
        spRight.setResizeWeight(0.9);
        spRight.setDividerLocation(0.9);
        spRight.setContinuousLayout(true);

        JSplitPane splitPane = new JSplitPane();
        splitPane.setLeftComponent(spLeft);
        splitPane.setRightComponent(spRight);
        splitPane.setResizeWeight(0.7);
        splitPane.setDividerLocation(0.7);

        frame.add(splitPane);

        // Force stdout and stderr to redirect to
        // Our game frame UI
        TextAreaOutputStream os1 = new TextAreaOutputStream(gameArea, 45);
        TextAreaOutputStream os2 = new TextAreaOutputStream(chatArea, 45);
        PrintStream ps1 = new PrintStream(os1);
        PrintStream ps2 = new PrintStream(os2);
        System.setOut(ps1);
        System.setErr(ps1);

        frame.pack();
        frame.setVisible(true);
        frame.setSize(WIDTH, HEIGHT);

        // Create our chat thread instance
        chatThread = new ChatThread(ps2);
        new Thread(chatThread).start();

        frame.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(WindowEvent winEvt) {
                // Clean exit
                System.out.println("Exiting...");
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        createGUI();
        start();
    }
}
