package com.e418.battleship.test_cli;

import com.e418.battleship.server.ServerConnectionInstance;
import com.e418.battleship.server.packetlib.client.ClientChat;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;

public class ChatThread implements Runnable {
    private PrintStream out;
    private PrintWriter sock;
    private Queue<String> inboundQueue;

    ChatThread(PrintStream out) {
        out.println("Instantiated chat thread...");
        this.out = out;
        this.sock = null;
        inboundQueue = new LinkedList<>();
    }

    public synchronized void setServerSocket(PrintWriter sock) {
        if (sock == null)
            out.println("[END GAME]");
        else
            out.println("[NEW GAME]");
        this.sock = sock;
    }

    public synchronized void enqueueChatData(String data) {
        inboundQueue.add(data);
    }

    public synchronized String dequeueChatData() {
        if (inboundQueue.isEmpty())
            return null;

        return inboundQueue.remove();
    }

    public synchronized void sendChatPacket(String message) {
        if (sock == null)
            return;

        ClientChat clientChat = new ClientChat();
        clientChat.setMessage(message);
        ServerConnectionInstance.writePacket(sock, clientChat);
    }

    @Override
    public void run() {
        out.println("Starting chat thread...");
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (sock == null) {
                continue;
            }

            // See if any new chat packets arrived
            String chat = dequeueChatData();
            while (chat != null) {
                // Add it to our chat box if so
                out.println(chat);
                chat = dequeueChatData();
            }
        }
    }
}
