package com.e418.battleship.test_cli;

import com.e418.battleship.server.ServerConnectionInstance;
import com.e418.battleship.server.ServerGameThread;
import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;
import com.e418.battleship.server.packetlib.client.PlaceHit;
import com.e418.battleship.server.packetlib.client.PlaceShips;
import com.e418.battleship.server.packetlib.server.GameOver;
import com.e418.battleship.server.packetlib.server.ServerChat;
import com.e418.battleship.server.packetlib.server.UpdateHits;

class Game {
    private MainMenu connection;
    // Markers
    private static final char OCEAN_MARKER = '#';
    private static final char SHIP_MARKER = 'S';
    private static final char MISS_MARKER = 'O';
    private static final char HIT_MARKER = 'X';
    // FIXME: Hard-coded game board for now...
    private static final int GAMEBOARD_SIZE = ServerGameThread.GAMEBOARD_SIZE;
    private char[][] gameBoard;

    Game(MainMenu connection) {
        this.connection = connection;
        gameBoard = new char[GAMEBOARD_SIZE][GAMEBOARD_SIZE];
        fillGameboard();
    }

    void startGame() {
        System.out.println("== WELCOME TO A NEW GAME OF BATTLESHIP ROYALE ==");
        System.out.println("Prepare to place your ships!");

        // Place ships!
        placeShips();

        // Loop around checking for packets until the game ends :)
        while (true) {
            BasePacket packet = connection.readPacket();

            switch (packet.getID()) {
                case GAME_START:
                    System.out.println("Game is full, let the fun begin!");
                    break;
                case UPDATE_HITS:
                    updateHits((UpdateHits)packet);
                    break;
                case SET_TURN:
                    takeTurn();
                    break;
                case SERVER_CHAT:
                    connection.addChat(((ServerChat)packet).getFormattedMessage());
                    break;
                case GAME_OVER:
                    gameOver((GameOver)packet);
                    return;
            }
        }
    }

    private void fillGameboard() {
        for (int y = 0; y < GAMEBOARD_SIZE; y++) {
            for (int x = 0; x < GAMEBOARD_SIZE; x++) {
                gameBoard[y][x] = OCEAN_MARKER;
            }
        }
    }

    private void gameOver(GameOver result) {
        System.out.println("== GAME OVER ==");

        if (result.didPlayerWin()) {
            System.out.println("CONGRATULATIONS: YOU WON!");
        } else {
            System.out.println("You lost. Better luck next time!");
        }

        // Reset chat thread
        connection.resetChat();
    }

    private void takeTurn() {
        int x;
        int y;

        System.out.println("It's your turn! Where do you want to fire?");

        while (true) {
            System.out.println("X:");
            Main.prompt();

            // Keep looking for chat packets
            // TODO: CLEANUP!
            // FIXME: STOP THREADS FROM BREAKING :/
            Thread t = new Thread() {
                public void run () {
                    while (!Thread.currentThread().isInterrupted()) {
                        BasePacket packet = connection.readPacket();
                        if (packet.getID() == PacketConstants.SERVER_CHAT)
                            connection.addChat(((ServerChat) packet).getFormattedMessage());
                    }
                }
            };

            //t.start();
            x = connection.input.nextInt();
            //t.stop();

            if (x < 0 || x >= GAMEBOARD_SIZE)
                continue;

            System.out.println("Y:");
            Main.prompt();
            y = connection.input.nextInt();

            if (y < 0 || y > GAMEBOARD_SIZE)
                continue;

            break;
        }

        // Send our hit up
        PlaceHit placeHit = new PlaceHit();
        placeHit.setCoords(x, y);

        ServerConnectionInstance.writePacket(connection.out, placeHit);
    }

    private void updateHits(UpdateHits new_hit) {
        // The marker we should use when adding the hit to the game board
        char marker = MISS_MARKER;

        System.out.print("Missile incoming! ");

        if (new_hit.isAHit()) {
            System.out.println("IT HIT A SHIP!");
            marker = HIT_MARKER;
        } else {
            System.out.println("It hit the water...");
        }

        // Update our game board map
        gameBoard[new_hit.getY()][new_hit.getX()] = marker;

        // Print it
        printGameboard();
    }

    private void placeShips() {
        PlaceShips.Battleship carrier = new PlaceShips.Battleship();
        PlaceShips.Battleship battleship = new PlaceShips.Battleship();
        PlaceShips.Battleship cruiser = new PlaceShips.Battleship();
        PlaceShips.Battleship submarine = new PlaceShips.Battleship();
        PlaceShips.Battleship destroyer = new PlaceShips.Battleship();

        while (true) {
            System.out.println("Valid coordinates are [0,0] to " +
                    "[" + (GAMEBOARD_SIZE - 1) + "," + (GAMEBOARD_SIZE - 1) + "]");

            System.out.println("Where do you want to place your carrier [5]");
            if (placeShipHelper(carrier, 5)) continue;

            System.out.println("Where do you want to place your battleship [4]");
            if (placeShipHelper(battleship, 4)) continue;

            System.out.println("Where do you want to place your cruiser [3]");
            if (placeShipHelper(cruiser, 3)) continue;

            System.out.println("Where do you want to place your submarine [3]");
            if (placeShipHelper(submarine, 3)) continue;

            System.out.println("Where do you want to place your destroyer [2]");
            if (placeShipHelper(destroyer, 2)) continue;

            System.out.println("Placement OK, your game board looks like this:");
            printGameboard();
            break;
        }

        PlaceShips placeShips = new PlaceShips();
        placeShips.setPlaces(carrier, battleship, cruiser, submarine, destroyer);

        // Send our placements up
        ServerConnectionInstance.writePacket(connection.out, placeShips);
    }

    private boolean placeShipHelper(PlaceShips.Battleship ship, int size) {
        System.out.println("X:");
        Main.prompt();
        ship.x = connection.input.nextInt();
        System.out.println("Y:");
        Main.prompt();
        ship.y = connection.input.nextInt();
        System.out.println("UP = 0; RIGHT = 1; DOWN = 2; LEFT = 3");
        Main.prompt();
        ship.dir = PlaceShips.BattleshipDirection.values()[connection.input.nextInt()];

        return !tryToPlace(ship, size);
    }

    private void printGameboard() {
        for (int y = 0; y < GAMEBOARD_SIZE; y++) {
            for (int x = 0; x < GAMEBOARD_SIZE; x++) {
                System.out.print(gameBoard[y][x] + "     ");
            }
            System.out.println();
            System.out.println();
        }
    }

    private boolean tryToPlace(PlaceShips.Battleship ship, int size) {
        try {
            for(int i = 0; i < size; i++) {
                switch (ship.dir) {
                    case UP:
                        gameBoard[ship.y + i][ship.x] = SHIP_MARKER;
                        break;
                    case DOWN:
                        gameBoard[ship.y - i][ship.x] = SHIP_MARKER;
                        break;
                    case LEFT:
                        gameBoard[ship.y ][ship.x + i] = SHIP_MARKER;
                        break;
                    case RIGHT:
                        gameBoard[ship.y][ship.x - i] = SHIP_MARKER;
                        break;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error, ship placement is invalid!");

            // Clear array and start over
            // FIXME
            gameBoard = new char[GAMEBOARD_SIZE][GAMEBOARD_SIZE];
            fillGameboard();
            return false;
        }

        // Placement OK
        return true;
    }
}
