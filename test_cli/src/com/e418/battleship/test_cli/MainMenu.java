package com.e418.battleship.test_cli;

import com.e418.battleship.server.ServerConnectionInstance;
import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketUtils;
import com.e418.battleship.server.packetlib.client.AuthRequest;
import com.e418.battleship.server.packetlib.client.JoinGame;
import com.e418.battleship.server.packetlib.client.ListGames;
import com.e418.battleship.server.packetlib.server.AuthResponse;
import com.e418.battleship.server.packetlib.server.JoinGameResponse;
import com.e418.battleship.server.packetlib.server.ListGamesResponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.UUID;

class MainMenu {
    Scanner input;
    private String username;
    private String token;
    private String gameServerIP;
    private int gameServerPort;
    private Socket sock;
    private BufferedReader in;
    private ChatThread chatThread;
    PrintWriter out;

    MainMenu(Scanner input, String username, String token,
             ChatThread chatThread, String gameServerIP, int gameServerPort) {
        this.input = input;
        this.username = username;
        this.token = token;
        this.chatThread = chatThread;
        this.gameServerIP = gameServerIP;
        this.gameServerPort = gameServerPort;
    }

    void showMainMenu() {
        boolean doLogout = false;

        // Connect to game server
        createSocketStreams();

        System.out.println("Authenticating with game server...");

        // Create our preauth packet
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername(username);
        authRequest.setToken(token);

        ServerConnectionInstance.writePacket(out, authRequest);

        // See what our auth result is
        AuthResponse authResponse = (AuthResponse)readPacket();
        if (authResponse.isAuthSuccess()) {
            System.out.println("Successfully authenticated with game server");
        } else {
            System.out.println("Error authenticating with game server!");
            System.exit(-1);
        }

        while (!doLogout) {
            System.out.println("MAIN MENU:\n1: Join game\n2: Create game\n3: Logout");
            Main.prompt();

            // Should we login or register
            int menuOption = input.nextInt();

            switch (menuOption) {
                case 1:
                    listGames();
                    break;
                case 2:
                    // Random UUID = new game
                    joinGame(UUID.randomUUID().toString());
                    break;
                case 3:
                    doLogout = true;
                default:
                    break;
            }
        }

        // Log out
        System.out.println("Logging out... Goodbye!");

        // Close our listener socket
        try {
            sock.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BasePacket readPacket() {
        // See what our response is
        try {
            String response = in.readLine();

            // Decode our packet
            return PacketUtils.decodePacket(response);
        } catch (Exception e) {
            // Error
            System.out.println("Error reading in packet!");
            e.printStackTrace();
            System.exit(-1);
        }

        // ?????
        return null;
    }

    public void addChat(String data) {
        chatThread.enqueueChatData(data);
    }

    public void resetChat() {
        chatThread.setServerSocket(null);
    }

    private void joinGame(String gameID) {
        // Create our join game packet
        JoinGame joinGame = new JoinGame();
        joinGame.setGameID(gameID);

        ServerConnectionInstance.writePacket(out, joinGame);

        // See if our join game worked
        JoinGameResponse joinGameResponse = (JoinGameResponse)readPacket();

        if (!joinGameResponse.isValid()) {
            System.out.println("Could not join game. Please try again later");
            return;
        }

        // Cool, we successfully joined a game!
        System.out.println("Successfully joined game: " + gameID);

        // Create our game chat thread socket
        chatThread.setServerSocket(out);

        Game game = new Game(this);
        game.startGame();
    }

    private void listGames() {
        // Create our list games packet
        ListGames listGames = new ListGames();

        ServerConnectionInstance.writePacket(out, listGames);

        // See what games are available
        ListGamesResponse listGamesResponse = (ListGamesResponse)readPacket();

        if (listGamesResponse.getGames().isEmpty()) {
            System.out.println("No joinable games currently :(");
            return;
        }

        // There are some joinable games!
        System.out.println("Which game do you want to join?");
        for (int i = 0; i < listGamesResponse.getGames().size(); i++) {
            System.out.println(i + ": " + listGamesResponse.getGames().get(i).split(",")[1]);
        }

        // Join it
        Main.prompt();
        int selected = input.nextInt();
        joinGame(listGamesResponse.getGames().get(selected).split(",")[0]);
    }

    private void createSocketStreams() {
        try {
            // Create our client socket
            sock = new Socket(gameServerIP, gameServerPort);

            in = new BufferedReader(
                    new InputStreamReader(sock.getInputStream()));
            out = new PrintWriter(
                    new OutputStreamWriter(sock.getOutputStream()));

            // Status message
            System.out.println("Connected to game server");
        } catch (Exception e) {
            System.out.println("Could not connect to game server!");
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
