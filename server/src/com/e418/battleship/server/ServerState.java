package com.e418.battleship.server;

public enum ServerState {
    PREAUTH, // Stuff you can only do when authenticating
    AUTH, // Stuff you can only do when authenticated
    INGAME // Stuff you can only do when in a game
}
