package com.e418.battleship.server.packetlib;

import java.io.Serializable;

/**
 * Our base packet class:
 * This holds the unique packet ID used
 * For serialization and deserialization
 */
public class BasePacket implements Serializable {
    private PacketConstants id;

    /**
     * Constructs a client/server packet
     * @param packet_id The ID we should make this packet
     */
    protected BasePacket(PacketConstants packet_id) {
        id = packet_id;
    }

    /**
     * Gets the ID of our packet
     * @return The ID of our packet
     */
    public PacketConstants getID() {
        return id;
    }
}
