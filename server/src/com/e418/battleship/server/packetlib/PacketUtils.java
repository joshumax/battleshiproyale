package com.e418.battleship.server.packetlib;

import javax.xml.bind.DatatypeConverter;
import java.io.*;

public class PacketUtils {

    /**
     * Decodes a packet from its base64 string
     * @param packet_data The string to decode
     * @return The decoded packet, or null on error
     */
    public static synchronized BasePacket decodePacket(String packet_data) {
        try {
            // Base64 decode
            byte[] decoded = DatatypeConverter.parseBase64Binary(packet_data);

            InputStream packetIS = new ByteArrayInputStream(decoded);

            // Read our packet in from the stream
            ObjectInputStream in = new ObjectInputStream(packetIS);

            // Return it
            return (BasePacket) in.readObject();
        } catch (Exception e) {
            // Something went wrong decoding the packet :(
            System.out.println("== ERROR DECODING PKT DATA: " + packet_data + " ==");
            e.printStackTrace();
        }

        // Error case
        return null;
    }

    /**
     * Encodes a packet to a base64 string
     * @param packet The packet to encode
     * @return A base64 string representing the encoded packet
     */
    public static synchronized String encodePacket(BasePacket packet) {
        try {
            ByteArrayOutputStream packetOS = new ByteArrayOutputStream();

            // Yay streams!
            ObjectOutputStream out = new ObjectOutputStream(packetOS);

            // Write our packet class to the byte array
            out.writeObject(packet);

            // Return base64 of array
            return DatatypeConverter.printBase64Binary(packetOS.toByteArray());
        } catch (Exception e) {
            // Something went wrong encoding the packet :(
            System.out.println("== ERROR ENCODING PKT @: " + packet + " ==");
            e.printStackTrace();
        }

        // Error case
        return null;
    }
}
