package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class AuthResponse extends BasePacket {
    private boolean authSuccess;

    public AuthResponse() {
        super(PacketConstants.AUTH_RESPONSE);
    }

    public boolean isAuthSuccess() {
        return authSuccess;
    }

    public void setAuthSuccess(boolean authSuccess) {
        this.authSuccess = authSuccess;
    }
}
