package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class SetTurn extends BasePacket {

    public SetTurn() {
        super(PacketConstants.SET_TURN);
    }
}
