package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

import java.util.ArrayList;
import java.util.List;

public class ListGamesResponse extends BasePacket {
    private List<String> games = new ArrayList<>();
    private final char DELIM = ',';

    public ListGamesResponse() {
        super(PacketConstants.LIST_GAMES_RESPONSE);
    }

    public void addGame(String id, String name) {
        // Form id,name
        games.add(id + DELIM + name);
    }

    public List<String> getGames() {
        return games;
    }
}
