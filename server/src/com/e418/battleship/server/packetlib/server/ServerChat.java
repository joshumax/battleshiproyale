package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class ServerChat extends BasePacket {
    private String username;
    private String message;

    public ServerChat() {
        super(PacketConstants.SERVER_CHAT);
    }

    public String getFormattedMessage() {
        return "<" + username + "> " + message;
    }

    public void setMessage(String username, String message) {
        this.username = username;
        this.message = message;
    }
}
