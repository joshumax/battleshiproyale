package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class JoinGameResponse extends BasePacket {
    private boolean valid;

    public JoinGameResponse() {
        super(PacketConstants.JOIN_GAME_RESPONSE);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
