package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class GameOver extends BasePacket {
    private boolean playerWon;

    public GameOver() {
        super(PacketConstants.GAME_OVER);
    }

    public void setPlayerWon(boolean playerWon) {
        this.playerWon = playerWon;
    }

    public boolean didPlayerWin() {
        return playerWon;
    }
}
