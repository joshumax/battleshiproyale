package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class GameStart extends BasePacket {

    public GameStart() {
        super(PacketConstants.GAME_START);
    }
}
