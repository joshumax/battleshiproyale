package com.e418.battleship.server.packetlib.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class UpdateHits extends BasePacket {
    private int x;
    private int y;
    private boolean isHit;

    public UpdateHits() {
        super(PacketConstants.UPDATE_HITS);
    }

    public void setCoords(int x, int y, boolean isHit) {
        this.x = x;
        this.y = y;
        this.isHit = isHit;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isAHit() {
        return isHit;
    }
}
