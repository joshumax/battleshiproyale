package com.e418.battleship.server.packetlib;

public enum PacketConstants {
    NULL_PACKET, // Null packet
    AUTH_REQUEST, // C->S Authentication request
    AUTH_RESPONSE, // S->C Authentication response
    LIST_GAMES, // C->S List all open games
    LIST_GAMES_RESPONSE, // S->C List games response
    JOIN_GAME, // C->S Join a game
    JOIN_GAME_RESPONSE, // S->C Join a game response
    PLACE_SHIPS, // C->S Place the ship positions
    GAME_START, // S->C Signals that a game has started
    SET_TURN, // S->C Signals that it's a player's turn
    PLACE_HIT, // C->S Sets a hit x,y coordinate during a player's turn
    UPDATE_HITS, // S->C Updates all player's screens with a good/bad hit
    GAME_OVER, // S->C Tells all players the game is over and the winner
    QUIT_GAME, // C->S Tells the server that the client wants to quit
              //      This will also trigger a GAME_OVER if players left = 1
    CLIENT_CHAT, // C->S Chat packet from a user
    SERVER_CHAT // S->C Sends the chat packet to all users
}
