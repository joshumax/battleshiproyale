package com.e418.battleship.server.packetlib.client;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

import java.io.Serializable;

public class PlaceShips extends BasePacket {
    /**
     * Forward is the bow of the ship
     */
    public enum BattleshipDirection {
        UP,
        RIGHT,
        DOWN,
        LEFT
    }

    public static final class Battleship implements Serializable {
        public int x;
        public int y;
        public BattleshipDirection dir;
    }

    private Battleship carrier;
    private Battleship battleship;
    private Battleship cruiser;
    private Battleship submarine;
    private Battleship destroyer;

    public PlaceShips() {
        super(PacketConstants.PLACE_SHIPS);
    }

    public void setPlaces(Battleship carrier, Battleship battleship,
                          Battleship cruiser, Battleship submarine, Battleship destroyer) {
        this.carrier = carrier;
        this.battleship = battleship;
        this.cruiser = cruiser;
        this.submarine = submarine;
        this.destroyer = destroyer;
    }

    public Battleship getCarrier() {
        return carrier;
    }

    public Battleship getBattleship() {
        return battleship;
    }

    public Battleship getCruiser() {
        return cruiser;
    }

    public Battleship getSubmarine() {
        return submarine;
    }

    public Battleship getDestroyer() {
        return destroyer;
    }
}
