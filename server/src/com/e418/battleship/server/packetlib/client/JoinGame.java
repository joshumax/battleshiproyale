package com.e418.battleship.server.packetlib.client;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class JoinGame extends BasePacket {
    private String gameID;

    public JoinGame() {
        super(PacketConstants.JOIN_GAME);
    }

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }
}
