package com.e418.battleship.server.packetlib.client;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class PlaceHit extends BasePacket {
    private int x;
    private int y;

    public PlaceHit() {
        super(PacketConstants.PLACE_HIT);
    }

    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
