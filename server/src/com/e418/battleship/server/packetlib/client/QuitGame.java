package com.e418.battleship.server.packetlib.client;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class QuitGame extends BasePacket {

    public QuitGame() {
        super(PacketConstants.QUIT_GAME);
    }
}
