package com.e418.battleship.server.packetlib.client;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;

public class ListGames extends BasePacket {
    public ListGames() {
        super(PacketConstants.LIST_GAMES);
    }
}
