package com.e418.battleship.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketConstants;
import com.e418.battleship.server.packetlib.client.ClientChat;
import com.e418.battleship.server.packetlib.client.PlaceHit;
import com.e418.battleship.server.packetlib.client.PlaceShips;
import com.e418.battleship.server.packetlib.server.*;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * This thread holds the states/actions of a currently
 * running game
 */
public class ServerGameThread implements Runnable {
    // Our game ID
    private String gameID;
    // This holds a reference to all of the playing clients
    private List<ServerConnectionInstance> players;
    // This is the maximum number of players that can join a game
    private static final int MAX_PLAYERS = 2;
    // This holds what state of the game we're in
    private enum GAME_STATE {
        SETUP, // Waiting for players to place ships
        GAME // Actual gameplay
    }
    private GAME_STATE currentState;
    // This holds what player's turn it is
    private int playerTurn = 0;
    // Should we keep the game up?
    private boolean keepGoing = true;

    /**
     * TODO: Game board size is currently hard-coded at 20x20;
     * in the future we may want to make the size editable.
     * We should go about fixing this later on -Josh
     */
    public static final int GAMEBOARD_SIZE = 20;

    public ServerGameThread(String gameID, ServerConnectionInstance initial_player) {
        this.gameID = gameID;
        players = new ArrayList<>();
        addPlayer(initial_player);

        // Set our initial game state to SETUP
        currentState = GAME_STATE.SETUP;
    }

    /**
     * Our thread entry point
     */
    @Override
    public void run() {
        int oldPlayerTurn = -1;

        log("Starting new game thread in SETUP state");

        // Loop around checking for packets while all players are alive
        while (checkAlivePlayers() && keepGoing) {
            // We're in game SETUP phase
            if (currentState == GAME_STATE.SETUP) {
                // Check if everyone has placed their battleships yet
                checkPlacedBattleships();
            }

            // We're in actual GAME phase
            else if (currentState == GAME_STATE.GAME) {
                // Let a player know it's their turn
                if (playerTurn != oldPlayerTurn) {
                    log("Telling player " + playerTurn + " it's their turn");
                    SetTurn setTurn = new SetTurn();
                    players.get(playerTurn).writePacket(setTurn);
                    oldPlayerTurn = playerTurn;
                }

                // Check for any chat packets
                boolean processedChat = false;
                for (ServerConnectionInstance player : players) {
                    if (player.peekPacketInQueue() == null
                            || player.peekPacketInQueue().getID() != PacketConstants.CLIENT_CHAT)
                        continue;

                    // We got a chat packet
                    ClientChat packet = (ClientChat)player.removePacketFromQueue();

                    // Log it
                    log("Got chat from player, text: " + packet.getMessage());

                    // Forward packet to all players
                    ServerChat serverChat = new ServerChat();
                    serverChat.setMessage(player.getUsername(), packet.getMessage());
                    sendPacketToAllPlayers(serverChat);
                    processedChat = true;
                }

                // A player may have sent >1 chat packet
                // Keep going until all chat messages are processed
                // TODO: Make this cleaner than what we're currently doing
                if (processedChat)
                    continue;

                // Poll for a game play packet
                BasePacket packet = players.get(playerTurn).removePacketFromQueue();

                // Did we get anything from the packet?
                if (packet != null && packet.getID() == PacketConstants.PLACE_HIT) {
                    // Process the player's hit response, if any
                    processHitPacket((PlaceHit)packet);
                }
            }

            // Sleep a bit so we don't burn the CPU
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Someone left
        System.out.println("A player left... Ending the game");

        // Send end game packet
        GameOver gameOver = new GameOver();
        gameOver.setPlayerWon(true);
        sendPacketToAllPlayers(gameOver);

        // Reset state
        for (ServerConnectionInstance player : players) {
            player.setState(ServerState.AUTH);
        }
    }

    /**
     * Gets the game ID of this game
     * @return The ID of the game
     */
    public String getGameID() {
        return gameID;
    }

    /**
     * Gets whether the game is open (joinable) or closed
     * @return true if open, false otherwise
     */
    public boolean isGameJoinable() {
        // If there's an open slot, we're still joinable
        return players.size() < MAX_PLAYERS;
    }

    /**
     * Adds a new player to the game
     * @param new_player The new player to add to the game
     */
    public void addPlayer(ServerConnectionInstance new_player) {
        // Create the player's gameboard (defaults to falses)
        new_player.gameBoard = new boolean[GAMEBOARD_SIZE][GAMEBOARD_SIZE];

        // Add them to the list
        players.add(new_player);
    }

    /**
     * Logger utility for game thread
     * @param text The text to log
     */
    private void log(String text) {
        System.out.println(gameID + ": " + text);
    }

    private boolean checkAlivePlayers() {
        List<ServerConnectionInstance> deadClients = new ArrayList<>();

        for (ServerConnectionInstance player : players) {
            if (!player.isClientAlive()) {
                // Someone died
                log("Reaping a dead client from the game :(");
                deadClients.add(player);
            }
        }

        if (deadClients.isEmpty()) {
            // Everyone is alive and healthy!
            return true;
        }

        // Time to fold this game -.-
        return false;
    }

    private void processHitPacket(PlaceHit placeHit) {
        // FIXME: Do bounds checking to make sure the X and Y is valid!
        boolean res = checkIsAHit(placeHit.getX(), placeHit.getY());

        // FIXME: THIS IS ONLY FOR 2P MODE RIGHT NOW!
        List<ServerConnectionInstance> losers = scanForLosers();

        if (!losers.isEmpty()) {
            // Somebody lost this round
            GameOver gameOver = new GameOver();
            gameOver.setPlayerWon(false);

            for (ServerConnectionInstance loser : losers) {
                loser.writePacket(gameOver);
            }

            // Kind of an ugly hack, but it works
            keepGoing = false;
        } else {
            // Update the hits for all clients to see
            UpdateHits updateHits = new UpdateHits();
            updateHits.setCoords(placeHit.getX(), placeHit.getY(), res);
            sendPacketToAllPlayers(updateHits);

            // Set another player's turn
            playerTurn = (playerTurn + 1) % MAX_PLAYERS;
        }
    }

    private List<ServerConnectionInstance> scanForLosers() {
        // FIXME: THIS IS ONLY FOR 2P MODE RIGHT NOW!
        List<ServerConnectionInstance> losers = new ArrayList<>();

        for (ServerConnectionInstance player : players) {
            int num_true = 0;
            for (int y = 0; y < GAMEBOARD_SIZE; y++) {
                for (int x = 0; x < GAMEBOARD_SIZE; x++) {
                    if (player.gameBoard[y][x]) {
                        // Still at least 1 ship afloat
                        num_true++;
                    }
                }
            }

            if (num_true == 0) {
                // This player has just lost
                losers.add(player);
            }
        }

        return losers;
    }

    private boolean checkIsAHit(int x, int y) {
        boolean res = false;
        // Check all the game boards to see if we hit anything
        for (ServerConnectionInstance player : players) {
            if (player.gameBoard[y][x]) {
                log("Got hit on a player gameboard");
                // Update the player's gameboard
                player.gameBoard[y][x] = false;
                res = true;
            }
        }
        return res;
    }

    private void checkPlacedBattleships() {
        if (players.size() < MAX_PLAYERS) {
            // We can't start the game, not enough people...
            return;
        }

        boolean shouldUpdateGameState = true;

        for (ServerConnectionInstance player : players) {
            BasePacket packet = player.removePacketFromQueue();

            if (packet != null) {
                // We got a new packet from this player! Is it place ships?
                if (packet.getID() == PacketConstants.PLACE_SHIPS) {
                    log("Got a place ship packet from client " + player.toString());
                    PlaceShips placeShips = (PlaceShips)packet;

                    // Place 'em!
                    processGameboard(player, placeShips.getCarrier(), 5);
                    processGameboard(player, placeShips.getBattleship(), 4);
                    processGameboard(player, placeShips.getCruiser(), 3);
                    processGameboard(player, placeShips.getSubmarine(), 3);
                    processGameboard(player, placeShips.getDestroyer(), 2);

                    // We have our ships now!
                    player.setPlacedBattleships();
                }
            }

            // Check if we've placed our battleships
            if (!player.hasPlacedBattleships()) {
                shouldUpdateGameState = false;
            }
        }

        // Has EVERYONE placed their battleships?
        if (shouldUpdateGameState) {
            log("All battleships have been placed! Updating game state!");
            currentState = GAME_STATE.GAME;

            // Send a game start packet to all players!
            GameStart gameStart = new GameStart();
            sendPacketToAllPlayers(gameStart);
        }
    }

    private void processGameboard(ServerConnectionInstance player, PlaceShips.Battleship ship, int size) {
        for(int i = 0; i < size; i++) {
            switch (ship.dir) {
                case UP:
                    player.gameBoard[ship.y + i][ship.x] = true;
                    break;
                case DOWN:
                    player.gameBoard[ship.y - i][ship.x] = true;
                    break;
                case LEFT:
                    player.gameBoard[ship.y ][ship.x + i] = true;
                    break;
                case RIGHT:
                    player.gameBoard[ship.y][ship.x - i] = true;
                    break;
            }
        }
    }

    private void sendPacketToAllPlayers(BasePacket packet) {
        log("Sending response packet to all players");

        for (ServerConnectionInstance player : players) {
            if (player.isClientAlive()) {
                player.writePacket(packet);
            }
        }
    }
}
