package com.e418.battleship.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class ServerMain {
    // The port to listen on
    public static final int PORT = 4545;
    // Our currently running games (id -> ServerGameThread)
    private static ConcurrentHashMap<String, ServerGameThread> games
            = new ConcurrentHashMap<>();

    @SuppressWarnings("all")
    public static void main(String[] args) {
        try (ServerSocket ssock = new ServerSocket(PORT)) {
            System.out.println("Starting Battleship Royale server...");

            while (true) {
                Socket sock = ssock.accept();
                System.out.println("Connected to new client");

                // Create our new connection instance
                new Thread(new ServerConnectionInstance(sock)).start();
            }

        } catch (IOException e) {
            // Something very bad happened!
            System.out.println("ERROR STARTING BATTLESHIP ROYALE SERVER!");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Gets a reference to the map of running game instances
     * @return The map of running game instances
     */
    public static synchronized ConcurrentHashMap<String, ServerGameThread> getGames() {
        return games;
    }
}
