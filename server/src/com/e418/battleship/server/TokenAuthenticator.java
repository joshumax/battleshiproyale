package com.e418.battleship.server;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TokenAuthenticator {
    private String username;
    private String token;
    private CloseableHttpClient httpClient;
    private String AUTH_BASE;

    /**
     * Creates a new token authenticator
     * @param _username The username to verify
     * @param _token The token to verify
     */
    public TokenAuthenticator(String _username, String _token) {
        username = _username;
        token = _token;
        httpClient = HttpClients.createDefault();
        AUTH_BASE = "http://127.0.0.1:9639";
    }

    /**
     * Parse a JSON string into a JSONObject
     * @param json The JSON
     * @return A new JSONObject
     * @throws ParseException The exception
     */
    private JSONObject parseJson(String json) throws ParseException {
        JSONParser parser = new JSONParser();
        return (JSONObject)parser.parse(json);
    }

    /**
     * Validate our session token
     * @return true if valid, false otherwise
     */
    public boolean validateToken() {
        System.out.println("> Validating token at: " + AUTH_BASE);
        HttpGet request = new HttpGet(AUTH_BASE + "/verify?username=" + username + "&token=" + token);

        // Create our validation request
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);

            // Now let's parse the resulting json
            JSONObject json = parseJson(content);

            // Is our token a valid one?
            return json.get("status").equals("success");
        } catch (Exception e) {
            // Error :(
            System.out.println("> Error connecting to auth server!");
            return false;
        }
    }
}
