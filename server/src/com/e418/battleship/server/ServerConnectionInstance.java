package com.e418.battleship.server;

import com.e418.battleship.server.packetlib.BasePacket;
import com.e418.battleship.server.packetlib.PacketUtils;
import com.e418.battleship.server.packetlib.client.AuthRequest;
import com.e418.battleship.server.packetlib.client.JoinGame;
import com.e418.battleship.server.packetlib.client.ListGames;
import com.e418.battleship.server.packetlib.server.AuthResponse;
import com.e418.battleship.server.packetlib.server.JoinGameResponse;
import com.e418.battleship.server.packetlib.server.ListGamesResponse;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class ServerConnectionInstance implements Runnable {
    private Socket sock;
    private ServerState state;
    // Our current game instance
    private ServerGameThread currentGame;
    // Is our client alive?
    private boolean clientAlive;
    // Our PrintWriter
    private PrintWriter pw_ref;
    // Our username
    private String username;
    ////////////////////////////////////
    // Have we placed our battleships?
    private boolean placedBattleships;
    // Our packet queue for the game thread
    private Queue<BasePacket> packetQueue;
    // Our gameboard
    public boolean[][] gameBoard;
    ////////////////////////////////////

    public ServerConnectionInstance(Socket s) {
        // Set default state
        state = ServerState.PREAUTH;
        sock = s;
        currentGame = null;
        clientAlive = true;
        placedBattleships = false;
        packetQueue = new LinkedList<>();
    }

    /**
     * Adds a packet to the packet queue buffer
     * @param packet The packet to add to the queue
     */
    public synchronized void addPacketToQueue(BasePacket packet) {
        packetQueue.add(packet);
    }

    /**
     * Removes the next element in the queue for processing
     * @return The packet if one is available; null otherwise
     */
    public synchronized BasePacket removePacketFromQueue() {
        if (packetQueue.isEmpty()) {
            // Nothing is in the packet queue
            return null;
        }

        return packetQueue.remove();
    }

    public synchronized BasePacket peekPacketInQueue() {
        if (packetQueue.isEmpty()) {
            // Nothing is in the packet queue
            return null;
        }

        return packetQueue.peek();
    }

    /**
     * Gets our player's username
     * @return The player's username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Checks whether we as a player have placed battleships
     * @return true if we have, false otherwise
     */
    public synchronized boolean hasPlacedBattleships() {
        return placedBattleships;
    }

    /**
     * Sets the fact that we have placed battleships
     */
    public synchronized void setPlacedBattleships() {
        placedBattleships = true;
    }

    /**
     * Lets other threads know if this client is alive
     * @return
     */
    public synchronized boolean isClientAlive() {
        return clientAlive;
    }

    /**
     * Sets ourselves to dead
     */
    private synchronized void setClientDead() {
        clientAlive = false;
    }

    /**
     * Gets the current client connection socket (used by ServerGameThread)
     * @return The client connection socket
     */
    public Socket getSocket() {
        return sock;
    }

    /**
     * Sets the server into a new state (used by ServerGameThread)
     * @param new_state The new server state
     */
    public void setState(ServerState new_state) {
        state = new_state;
    }

    @Override
    public void run() {
        System.out.println("Accepted client: " + sock.getInetAddress().getHostName());

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
             PrintWriter out = new PrintWriter(
                     new OutputStreamWriter(sock.getOutputStream()))) {
            // Save our PrintWriter reference
            pw_ref = out;

            // Okay, so we created our in <-> out streams to/from the client, poll for data!
            while (true) {
                String in_str = in.readLine();

                if (in_str == null) {
                    System.out.println("> Socket closed, ending connection...");
                    // End loop
                    break;
                }

                // Decode our packet from the base64+\n
                BasePacket packet = PacketUtils.decodePacket(in_str);

                if (packet == null) {
                    System.out.println("> E: Could not decode packet, result null!");
                    continue;
                }

                // PREAUTH STATE: ONLY ACCEPT AUTH PACKETS
                if (state == ServerState.PREAUTH) {
                    processPreauth(out, packet);
                }

                // AUTH STATE: ONLY PROCESS AUTH PACKETS
                else if (state == ServerState.AUTH) {
                    processAuth(out, packet);
                }

                // INGAME STATE: ONLY FORWARD INGAME PACKETS
                else if (state == ServerState.INGAME) {
                    processIngame(out, packet);
                }
            }
        } catch (Exception e) {
            // Something broke
            e.printStackTrace();
        } finally {
            // Set ourselves as dead
            setClientDead();

            // Clean up any games we left from the open games list
            cleanupGameList();

            // Close our listener socket
            try {
                sock.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * If we disconnect after joining a game, that game is no longer playable
     * So we should clear it from the games list after the game server folds
     * TODO: This may not work in the future, depending on where we go with the game
     */
    private void cleanupGameList() {
        if (currentGame == null) {
            return;
        }

        // We were in a game; remove it
        System.out.println("Game " + currentGame.getGameID() + " found, clearing...");
        ServerMain.getGames().remove(currentGame.getGameID());
    }

    /**
     * Processes INGAME packets (just queue them up in the current ServerGameThread)
     * @param out out
     * @param packet packet
     */
    private void processIngame(PrintWriter out, BasePacket packet) {
        // Enqueue our packet
        System.out.println("> Got a new game packet from client, enqueueing...");
        addPacketToQueue(packet);
    }

    /**
     * Processes AUTH packets
     * @param out out
     * @param packet packet
     */
    private void processAuth(PrintWriter out, BasePacket packet) {
        switch (packet.getID()) {
            // We got a list games request
            case LIST_GAMES:
                System.out.println("> Got list games packet from client");

                // Create our response packet
                ListGamesResponse listGamesResponse = new ListGamesResponse();

                // Loop through all the games and see which ones are open
                for (Map.Entry<String, ServerGameThread> entry : ServerMain.getGames().entrySet()) {
                    if (entry.getValue().isGameJoinable()) {
                        System.out.println("> - Game ID joinable: " + entry.getKey());

                        // Add this game to our list
                        listGamesResponse.addGame(entry.getKey(), "G-" + entry.getKey());
                    }
                }

                // Write our packet back
                writePacket(out, listGamesResponse);
                break;
            case JOIN_GAME:
                System.out.println("> Got join game packet from client");

                // Check the game ID the user wants to join/create
                JoinGame joinRequest = (JoinGame)packet;

                // FIXME: Check if gameID is a valid game ID when creating a game
                String gameID = joinRequest.getGameID();

                // Create a join game response packet
                JoinGameResponse joinGameResponse = new JoinGameResponse();
                joinGameResponse.setValid(false);

                // Need to synchronize so that nobody else can join while we're joining!
                synchronized (ServerMain.getGames()) {
                    if (ServerMain.getGames().containsKey(gameID)) {
                        // User wants to join an existing game
                        ServerGameThread game = ServerMain.getGames().get(gameID);

                        if (game.isGameJoinable()) {
                            // the game is joinable!
                            System.out.println("> Game " + gameID + " is joinable! Switching to INGAME");
                            joinGameResponse.setValid(true);

                            // Add ourselves to the open game
                            game.addPlayer(this);
                            currentGame = game;
                            state = ServerState.INGAME;
                        }
                    } else {
                        // User wants to create a new game with ID = gameID
                        System.out.println("> Creating new game with ID: " + gameID + ". Switching to INGAME");
                        joinGameResponse.setValid(true);

                        // Add it to the list and set us as the initial player
                        ServerGameThread new_game = new ServerGameThread(gameID, this);
                        ServerMain.getGames().put(gameID, new_game);

                        // Start up the game instance thread
                        new Thread(new_game).start();
                        currentGame = new_game;
                        state = ServerState.INGAME;
                    }
                }

                // Write our packet back
                writePacket(out, joinGameResponse);
                break;
            default:
                System.out.println("> E: Got UNKNOWN packet during AUTH state!");
                break;
        }
    }

    /**
     * Processes PREAUTH packets
     * @param out out
     * @param packet packet
     */
    private void processPreauth(PrintWriter out, BasePacket packet) {
        switch (packet.getID()) {
            // We got an authentication request
            case AUTH_REQUEST:
                System.out.println("> Got authentication request packet from client");

                // Check our session token
                AuthRequest authRequest = (AuthRequest)packet;
                TokenAuthenticator authenticator = new TokenAuthenticator(authRequest.getUsername(),
                        authRequest.getToken());

                // Create our response packet
                AuthResponse authResponse = new AuthResponse();
                authResponse.setAuthSuccess(false);

                if (authenticator.validateToken()) {
                    // The token is valid
                    System.out.println("> Got a valid token for user, switching to AUTH");
                    authResponse.setAuthSuccess(true);
                    username = authRequest.getUsername();
                    state = ServerState.AUTH;
                } else {
                    System.out.println("> E: User token is not valid, please try again");
                }

                // Write our packet back
                writePacket(out, authResponse);
                break;
            case NULL_PACKET:
                System.out.println("> E: Got null packet during PREAUTH");
                break;
            // What the heck is the client sending during PREAUTH?????
            default:
                System.out.println("> E: Got UNKNOWN packet during PREAUTH state!");
                break;
        }
    }

    /**
     * Packet writer helper function
     * @param w The PrintWriter to write with
     * @param packet The packet to write
     */
    public static void writePacket(PrintWriter w, BasePacket packet) {
        // Encode our packet
        String out = PacketUtils.encodePacket(packet);

        // Print it plus \n
        w.println(out);
        w.flush();
    }

    /**
     * Packet writer helper function
     * @param packet The packet to write
     */
    public void writePacket(BasePacket packet) {
        writePacket(pw_ref, packet);
    }
}
