package com.e418.battleship.authserver;

import com.e418.battleship.authserver.providers.IStorageProvider;
import com.e418.battleship.authserver.providers.SimpleFileProvider;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Our main Yggdrasil instance
 */
public class YggInstance {
    // TODO: Allow changing the provider!
    private IStorageProvider provider = new SimpleFileProvider();
    // Our session manager instance
    private SessionManager sessions;

    YggInstance() {
        this.sessions = new SessionManager();
    }

    /**
     * Creates a new user from the user/password combo
     * @param username The username
     * @param password The password
     * @return The USER_CREATE result of the attempt
     */
    public IStorageProvider.USER_CREATE createUser(String username, String password) {
        // Is this actually a valid user/pass combo?
        if (username == null || password == null || !checkValid(username) || !checkValid(password)) {
            // Invalid combo
            return IStorageProvider.USER_CREATE.UNKNOWN_ERROR;
        }

        // Create a new UserInfo object
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername(username);
        userInfo.setPassword(hashPassword(password));

        // Now save out the new user (hopefully)
        return this.provider.updateUserData(userInfo);
    }

    /**
     * Log into the service
     * @param username The user to log into
     * @param password The user's password to validate
     * @return true on correct credentials, false otherwise
     */
    public boolean login(String username, String password) {
        if (username == null || password == null) {
            return false;
        }

        UserInfo userInfo = this.provider.getInfoForUser(username);

        // Does the user exist in our DB?
        if (userInfo == null) {
            return false;
        }

        // Is our password correct?
        if (!userInfo.getPassword().equals(hashPassword(password))) {
            return false;
        }

        // Create a session token
        this.sessions.generateNewSession(username);
        return true;
    }

    /**
     * Gets the session token for the specified user
     * @param username The user to get the session token of
     * @return The session token, or null if one does not exist
     */
    public String getUserToken(String username) {
        return this.sessions.getSessionToken(username);
    }

    /**
     * Logs out of the service
     * @param token The token to validate the logout against
     */
    public void logout(String token) {
        if (token == null) {
            return;
        }

        // Clear any tokens for this user in memory
        this.sessions.clearToken(this.sessions.getUserForToken(token));
    }

    /**
     * Very basic string sanitization checks
     * @param text The text to check for validity
     * @return true if the text does not contain invalid chars, or false otherwise
     */
    private boolean checkValid(String text) {
        return !text.contains(":") && !text.contains("\r") &&
                !text.contains("\n") && !text.contains("\\");
    }

    /**
     * Creates a hash of the specified password
     * @param password The password to hash
     * @return The password hash
     */
    // TODO: Add salts?
    private String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] res = md.digest(password.getBytes(StandardCharsets.UTF_8));

            // Convert byte array into sig-num representation
            BigInteger number = new BigInteger(1, res);

            // Convert message digest into hex value
            StringBuilder hexString = new StringBuilder(number.toString(16));

            // Pad with leading zeros
            while (hexString.length() < 32) {
                hexString.insert(0, '0');
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Hash algorithm not available! Storing passwords IN PLAINTEXT!");
        }

        // Uh-oh
        return password;
    }
}
