package com.e418.battleship.authserver;

import com.e418.battleship.authserver.providers.IStorageProvider;
import fi.iki.elonen.NanoHTTPD;
import java.io.IOException;
import java.util.Map;
import org.json.simple.JSONObject;

public class AuthMain extends NanoHTTPD {
    // Yggdrasil port
    private static final int PORT = 9639;
    // Our singleton Ygg instance
    private static YggInstance yggInstance;

    private AuthMain() throws IOException {
        // Set up our port
        super(PORT);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        System.out.println("== Running Yggdrasil on http://localhost:" + PORT + " ==");
    }

    public static void main(String[] args) {
        yggInstance = new YggInstance();

        try {
            new AuthMain();
        } catch (IOException e) {
            System.out.println("Couldn't start Yggdrasil server: " + e);
        }
    }

    /**
     * Server instance
     * @param session An HTTP request
     * @return An HTTP response for the given request
     */
    @Override
    public Response serve(IHTTPSession session) {
        String URI = session.getUri();
        Map<String, String> params = session.getParms();
        String body = "";

        if (URI.startsWith("/login") && params.size() == 2) {
            // Login
            body += doLogin(params.get("username"), params.get("password"));
        } else if (URI.startsWith("/logout") && params.size() == 1) {
            // Logout
            body += doLogout(params.get("token"));
        } else if (URI.startsWith("/register") && params.size() == 2) {
            // Register
            body += doRegister(params.get("username"), params.get("password"));
        } else if (URI.startsWith("/verify") && params.size() == 2) {
            // Verify
            body += doVerify(params.get("username"), params.get("token"));
        } else {
            body += "<html><head><body><h1>Battleship Royale Authentication Server</h1></body></html>";
        }

        // Now we return our response
        return newFixedLengthResponse(body + "\n");
    }

    /**
     * Login handler
     * @param username The username
     * @param password The password
     * @return JSON
     */
    private String doLogin(String username, String password) {
        JSONObject result = new JSONObject();

        if (!yggInstance.login(username, password)) {
            result.put("status", "fail");
            return result.toJSONString();
        }

        // We successfully logged in!
        result.put("status", "success");
        result.put("token", yggInstance.getUserToken(username));
        return result.toJSONString();
    }

    /**
     * Logout handler
     * @param token The token
     * @return JSON
     */
    private String doLogout(String token) {
        JSONObject result = new JSONObject();

        yggInstance.logout(token);
        result.put("status", "success");
        return result.toJSONString();
    }

    /**
     * Verify handler
     * @param username The username
     * @param token The token
     * @return JSON
     */
    private String doVerify(String username, String token) {
        JSONObject result = new JSONObject();

        String user_token = yggInstance.getUserToken(username);
        if (user_token == null || !user_token.equals(token)) {
            // Token not found or does not match
            result.put("status", "fail");
            return result.toJSONString();
        }

        result.put("status", "success");
        return result.toJSONString();
    }

    /**
     * Register handler
     * @param username The username
     * @param password The password
     * @return JSON
     */
    private String doRegister(String username, String password) {
        JSONObject result = new JSONObject();

        if (yggInstance.createUser(username, password) != IStorageProvider.USER_CREATE.SUCCESS) {
            result.put("status", "fail");
            return result.toJSONString();
        }

        // We successfully registered!
        result.put("status", "success");
        return result.toJSONString();
    }
}
