package com.e418.battleship.authserver;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class manages session keys for logged-in users
 */
public class SessionManager {
    // Our dictionary of username -> session key mappings
    private Map<String, String> sessions;

    SessionManager() {
        this.sessions = new HashMap<>();
    }

    /**
     * Generates a new session token for a specified username
     * @param username The username to create a token for
     */
    public void generateNewSession(String username) {
        if (username == null) {
            return;
        }

        this.sessions.put(username, generateNewToken());
    }

    /**
     * Gets the session token associated with a given username
     * @param username The username to get a token for
     * @return The session token string, or null if none exists
     */
    public String getSessionToken(String username) {
        if (username == null) {
            return null;
        }

        return this.sessions.get(username);
    }

    /**
     * Find a username based on a token
     * @param token The token
     * @return The username for the specified token, or null if not found
     */
    public String getUserForToken(String token) {
        if (token == null) {
            return null;
        }

        for (Map.Entry<String, String> entry : this.sessions.entrySet()) {
            if (entry.getValue().equals(token)) {
                return entry.getKey();
            }
        }

        // No match
        return null;
    }

    /**
     * Clears the token saved for the specified user
     * @param username the username to clear the token for
     */
    public void clearToken(String username) {
        this.sessions.remove(username);
    }

    /**
     * Generates a new session token
     * @return A new random session token
     */
    private String generateNewToken() {
        return UUID.randomUUID().toString();
    }
}
