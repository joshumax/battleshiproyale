package com.e418.battleship.authserver;

public class UserInfo {
    private String username;
    private String password;

    public UserInfo() {
        this.username = null;
        this.password = null;
    }

    /**
     * Gets the current username of the returned user info, or null if none exists
     * @return The username, or null
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets the username of this object
     * @param name The username to set
     */
    public void setUsername(String name) {
        this.username = name;
    }

    /**
     * Gets the current password of the returned user info, or null if none exists
     * @return The password, or null
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password of this object
     * @param given_pass The password to set
     */
    public void setPassword(String given_pass) {
        this.password = given_pass;
    }

    /**
     * Validates whether a given password is equal to a user's password
     * @param given_pass The given password to check
     * @return true if validated, or false if the passwords do not match
     */
    public boolean validatePassword(String given_pass) {
        return this.password.equals(given_pass);
    }
}
