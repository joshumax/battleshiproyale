package com.e418.battleship.authserver.providers;

import com.e418.battleship.authserver.UserInfo;

/**
 * Interface for user and password storage providers
 */
public interface IStorageProvider {
    // This thing
    enum USER_CREATE {
        SUCCESS,
        ALREADY_EXISTS,
        UNKNOWN_ERROR
    }

    /**
     * Retrieve the user information from the backend with the specified user/password
     * @param username The username to search for
     * @return A UserInfo object representing the result, or null if the user is not found
     */
    UserInfo getInfoForUser(String username);

    /**
     * Update a user's data in the storage provider
     * @param user A !!SANITIZED!! UserInfo object representing the new data
     * @return USER_CREATE
     */
    USER_CREATE updateUserData(UserInfo user);
}
