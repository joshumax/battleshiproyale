package com.e418.battleship.authserver.providers;

import com.e418.battleship.authserver.UserInfo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SimpleFileProvider implements IStorageProvider {
    // Our file name
    private static final String DB_FILE_NAME = "YGGDRASIL_DB";

    // Our delimiter
    private static final String DELIM = ":";

    @Override
    public UserInfo getInfoForUser(String username) {
        if (username == null) {
            return null;
        }

        try (FileReader r = new FileReader(DB_FILE_NAME)) {
            Scanner cur = new Scanner(r);

            // Look for the requested username
            int current_line = 1;
            while (cur.hasNext()) {
                String[] user_pass_combo = cur.next().split(DELIM);

                if (user_pass_combo.length != 2) {
                    System.out.println("Could not parse " + current_line);
                    current_line++;
                    continue;
                }

                // Is our requested username at this line?
                if (username.equals(user_pass_combo[0])) {
                    System.out.println("Found user entry: " + username);

                    // Create a new user information object
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername(user_pass_combo[0]);
                    userInfo.setPassword(user_pass_combo[1]);

                    return userInfo;
                }

                // Keep looking!
                current_line++;
            }

        } catch (Exception e) {
            System.out.println("Error opening user DB file!");
        }

        System.out.println("Could not find entry for user: " + username);
        return null;
    }

    @Override
    // TODO: Implement updating user info rather than just appending it!
    public USER_CREATE updateUserData(UserInfo user) {
        System.out.println("=ADD USER=====================================");

        // Is our UserInfo object valid?
        if (user.getUsername() == null || user.getPassword() == null) {
            System.out.println("User info is invalid");
            System.out.println("==============================================");
            return USER_CREATE.UNKNOWN_ERROR;
        }

        // First check to see if the user already exists! (FIXME)
        if (getInfoForUser(user.getUsername()) != null) {
            System.out.println("User " + user.getUsername() + " already exists!");
            System.out.println("==============================================");
            return USER_CREATE.ALREADY_EXISTS;
        }

        File f = new File(DB_FILE_NAME);

        // Create a new database file if it doesn't exist
        try {
            if (!f.exists()) {
                f.createNewFile();
            }
        } catch (IOException e) {
            System.out.println("Could not create new file, continuing anyways...");
        }

        try (FileWriter w = new FileWriter(DB_FILE_NAME, true)) {
            // Append our user information to the file
            w.write(user.getUsername() + DELIM + user.getPassword());
            w.write(System.lineSeparator());
            w.flush();
        } catch (Exception e) {
            System.out.println("Could not create new entry for user: " + user.getUsername());
            System.out.println("==============================================");
            return USER_CREATE.UNKNOWN_ERROR;
        }

        // Success (hopefully)!
        System.out.println("Successfully added user " + user.getUsername());
        System.out.println("==============================================");
        return USER_CREATE.SUCCESS;
    }
}
