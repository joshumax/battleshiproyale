package com.e418.battleship.net;

public class GameListInfo {
    private String gameName;
    private String gameID;

    public GameListInfo(String name, String ID) {
        gameName = name;
        gameID = ID;
    }

    /**
     * Gets the name of an open game
     * @return The open game's name
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * Gets the ID of the open game
     * @return The ID of the open game
     */
    public String getGameID() {
        return gameID;
    }
}
