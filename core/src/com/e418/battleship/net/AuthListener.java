package com.e418.battleship.net;

public class AuthListener {

    /**
     * Callback for authentication results
     * @param c A reference to the auth client
     * @param success The status of the authentication call
     */
    public void result(AuthClient c, boolean success) {
        // Empty
    }
}
