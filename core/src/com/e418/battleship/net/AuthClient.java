package com.e418.battleship.net;

import com.e418.battleship.BattleshipRoyale;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class AuthClient implements Runnable {
    private String username;
    private String password;
    private AuthListener callback;
    private int authType;
    private CloseableHttpClient httpClient;
    private String AUTH_BASE;
    private String authToken;

    /**
     * Constructs a new authentication client
     * @param _user The username to authenticate with
     * @param _pass The password to authenticate with
     * @param cb A callback for our resulting data
     * @param auth The authentication mode (0 = login, 1 = register)
     */
    public AuthClient(String _user, String _pass, AuthListener cb, int auth) {
        username = _user;
        password = _pass;
        callback = cb;
        authType = auth;
        httpClient = HttpClients.createDefault();
        AUTH_BASE = BattleshipRoyale.getInstance().getAuthAddress();
        authToken = null;
    }

    public AuthClient(String _user, String _pass, String authBase) {
        username = _user;
        password = _pass;
        httpClient = HttpClients.createDefault();
        AUTH_BASE = authBase;
    }

    @Override
    public void run() {
        boolean status = false;
        System.out.println("Authenticating at: " + AUTH_BASE);

        // 0 for login mode
        if (authType == 0) {
            authToken = doLogin();

            // Did we get an auth token?
            if (authToken != null) {
                System.out.println("Success! Got token: " + authToken);
                status = true;
            } else {
                System.out.println("Error, invalid credentials!");
            }
        } else {
            // Register to the auth server
            status = doRegister();

            if (status) {
                System.out.println("Register success for user: " + username);
            } else {
                System.out.println("Error registering user!");
            }
        }

        // Callback time
        callback.result(this, status);
    }

    /**
     * Gets our username
     * @return Our username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets a resulting authentication token
     * @return The authentication token, or null if none
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Parse a JSON string into a JSONObject
     * @param json The JSON
     * @return A new JSONObject
     * @throws ParseException The exception
     */
    private JSONObject parseJson(String json) throws ParseException {
        JSONParser parser = new JSONParser();
        return (JSONObject)parser.parse(json);
    }

    /**
     * Login
     * @return The authentication token on success, or null on failure
     */
    public String doLogin() {
        HttpGet request = new HttpGet(AUTH_BASE + "/login?username=" + username + "&password=" + password);

        // Create our login request
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);

            // Now let's parse the resulting json
            JSONObject json = parseJson(content);

            if (!json.get("status").equals("success")) {
                // Invalid user/password
                return null;
            }

            // Successful login!
            return json.get("token").toString();
        } catch (Exception e) {
            // Error :(
            System.out.println("Error connecting to auth server!");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Register
     * @return true on successful registration, or false on failure
     */
    public boolean doRegister() {
        HttpGet request = new HttpGet(AUTH_BASE + "/register?username=" + username + "&password=" + password);

        // Create our login request
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            HttpEntity entity = response.getEntity();
            String content = EntityUtils.toString(entity);

            // Now let's parse the resulting json
            JSONObject json = parseJson(content);

            return json.get("status").equals("success");
        } catch (Exception e) {
            // Error :(
            System.out.println("Error connecting to auth server!");
            e.printStackTrace();
            return false;
        }
    }
}
