package com.e418.battleship;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;
import com.e418.battleship.screens.LoginScreen;

import java.awt.*;
import java.util.Locale;

public class BattleshipRoyale extends Game {
	// Our game skin
	private static Skin gameSkin;
	// Our game name
	private static final String GAME_NAME = "Battleship Royale";
	// Our game image
	private static Texture gameImage;
	// Our i18n stuff
	private static I18NBundle i18n;
	// Our singleton instance
	private static BattleshipRoyale ourInstance = null;
	// Username/token
	private static String username = null;
	private static String token = null;
	// Our auth server address
	private static String authAddress = "http://127.0.0.1:9639";
	
	@Override
	public void create () {
		// Create our singleton
		ourInstance = this;

		// Create our game skin
		gameSkin = new Skin(Gdx.files.internal("skin/clean-crispy-ui.json"));

		// Create our logo texture
		gameImage = new Texture(Gdx.files.internal("logo.png"));

		// Set up our i18n
		Locale locale = new Locale("en");
		i18n = I18NBundle.createBundle(Gdx.files.internal("i18n/GameStrings"), locale);

		// Set ourselves to the login screen
		setScreen(new LoginScreen());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		// EMPTY
	}

	/**
	 * Gets our username
	 * @return Our username
	 */
	public synchronized String getUsername() {
		return username;
	}

	/**
	 * Sets our username
	 * @param s Our username
	 */
	public synchronized void setUsername(String s) {
		username = s;
	}

	/**
	 * Gets our token
	 * @return Our token
	 */
	public synchronized String getToken() {
		return token;
	}

	/**
	 * Sets our token
	 * @param s Our token
	 */
	public synchronized void setToken(String s) {
		token = s;
	}

	/**
	 * Clears our username and authentication token info
	 */
	public void clearAuthInfo() {
		username = null;
		token = null;
	}

	/**
	 * Gets the game skin to be used
	 * @return The game skin to use
	 */
	public Skin getGameSkin() {
		return gameSkin;
	}

	/**
	 * Gets the name of our game
	 * @return The name of our battleship game
	 */
	public String getGameName()
	{
		return GAME_NAME;
	}

	/**
	 * Gets the logo for our game
	 * @return The texture logo of our battleship game
	 */
	public Texture getGameLogo()
	{
		return gameImage;
	}

	/**
	 * Gets the localized strings for our game
	 * @return The bundle of localized game strings
	 */
	public I18NBundle getStrings() {
		return i18n;
	}

	/**
	 * Gets our authentication server address
	 * @return Our yggdrasil server address
	 */
	public String getAuthAddress() {
		return authAddress;
	}

	/**
	 * Gets our singleton game instance
	 * @return Our game singleton
	 */
	public static BattleshipRoyale getInstance()
	{
		return ourInstance;
	}
}
