package com.e418.battleship.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.e418.battleship.BattleshipRoyale;
import com.e418.battleship.net.AuthClient;
import com.e418.battleship.net.AuthListener;

public class RegisterScreen implements Screen {
    private Stage stage;

    public RegisterScreen() {
        // Get our battleship game instance and create our screen
        final BattleshipRoyale game = BattleshipRoyale.getInstance();
        stage = new Stage(new ScreenViewport());

        // Create our Battleship Royale title
        Image title = new Image(game.getGameLogo());
        title.setScale((float)0.5);
        title.setPosition((int)(Gdx.graphics.getWidth() / 2 - title.getWidth() / 4), 0);
        title.setY(Gdx.graphics.getHeight() * 2 / 3);
        stage.addActor(title);

        // Create our username and password field
        final TextField usernameTextField = new TextField("", game.getGameSkin());
        usernameTextField.setMessageText(game.getStrings().get("new_user"));
        usernameTextField.setAlignment(Align.center);
        usernameTextField.setWidth(Gdx.graphics.getWidth() / 4);
        usernameTextField.setHeight(50);
        usernameTextField.setPosition((int)(Gdx.graphics.getWidth() / 2 - usernameTextField.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - usernameTextField.getHeight() / 2)
                        + usernameTextField.getHeight() * (float)1.25);
        stage.addActor(usernameTextField);

        final TextField passwordTextField = new TextField("", game.getGameSkin());
        passwordTextField.setMessageText(game.getStrings().get("new_pass"));
        passwordTextField.setPasswordMode(true);
        passwordTextField.setPasswordCharacter('*');
        passwordTextField.setAlignment(Align.center);
        passwordTextField.setWidth(Gdx.graphics.getWidth() / 4);
        passwordTextField.setHeight(50);
        passwordTextField.setPosition((int)(Gdx.graphics.getWidth() / 2 - passwordTextField.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - passwordTextField.getHeight() / 2));
        stage.addActor(passwordTextField);
        //////////

        // Create our register and back buttons
        TextButton registerButton = new TextButton(game.getStrings().get("enlist"), game.getGameSkin());
        registerButton.setWidth(Gdx.graphics.getWidth() / 4);
        registerButton.setHeight(50);
        registerButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - registerButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - registerButton.getHeight() / 2
                        - passwordTextField.getHeight() * (float)1.5));
        registerButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (usernameTextField.getText().isEmpty() || passwordTextField.getText().isEmpty()) {
                    Dialog dialog = new Dialog(game.getStrings().get("error"), game.getGameSkin()) {
                        public void result(Object obj) {}
                    };

                    dialog.text(game.getStrings().get("login_empty"));
                    dialog.button(game.getStrings().get("ok"), true);
                    dialog.show(stage);
                    return;
                }

                AuthClient client = new AuthClient(usernameTextField.getText(), passwordTextField.getText(),
                        new AuthListener(){
                            @Override
                            public void result(AuthClient c, boolean success) {
                                // Did we authenticate correctly?
                                if (!success) {
                                    Dialog dialog = new Dialog(game.getStrings().get("error"), game.getGameSkin()) {
                                        public void result(Object obj) {}
                                    };

                                    dialog.text(game.getStrings().get("unknown_error"));
                                    dialog.button(game.getStrings().get("ok"), true);
                                    dialog.show(stage);
                                    return;
                                }

                                // Success
                                Dialog dialog = new Dialog(game.getStrings().get("success"), game.getGameSkin()) {
                                    public void result(Object obj) {}
                                };

                                dialog.text(game.getStrings().get("register_successful"));
                                dialog.button(game.getStrings().get("ok"), true);
                                dialog.show(stage);
                            }
                        }, 1);

                Thread clientThread = new Thread(client);
                clientThread.start();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(registerButton);

        TextButton returnButton = new TextButton(game.getStrings().get("return"), game.getGameSkin());
        returnButton.setWidth(Gdx.graphics.getWidth() / 4);
        returnButton.setHeight(50);
        returnButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - returnButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - registerButton.getHeight() / 2
                        - passwordTextField.getHeight() * (float)1.25 * 2));
        returnButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new LoginScreen());
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(returnButton);
        //////////
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor((float)0.8, (float)1, (float)1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
