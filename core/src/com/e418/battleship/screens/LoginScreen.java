package com.e418.battleship.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.e418.battleship.BattleshipRoyale;
import com.e418.battleship.net.AuthClient;
import com.e418.battleship.net.AuthListener;

public class LoginScreen implements Screen {
    private Stage stage;
    private final BattleshipRoyale game;

    public LoginScreen() {
        // Get our battleship game instance and create our screen
        game = BattleshipRoyale.getInstance();
        stage = new Stage(new ScreenViewport());

        // Create our Battleship Royale title
        Image title = new Image(game.getGameLogo());
        title.setScale((float)0.5);
        title.setPosition((int)(Gdx.graphics.getWidth() / 2 - title.getWidth() / 4), 0);
        title.setY(Gdx.graphics.getHeight() * 2 / 3);
        stage.addActor(title);

        // Create our username and password field
        final TextField usernameTextField = new TextField("", game.getGameSkin());
        usernameTextField.setMessageText(game.getStrings().get("username"));
        usernameTextField.setAlignment(Align.center);
        usernameTextField.setWidth(Gdx.graphics.getWidth() / 4);
        usernameTextField.setHeight(50);
        usernameTextField.setPosition((int)(Gdx.graphics.getWidth() / 2 - usernameTextField.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - usernameTextField.getHeight() / 2)
                        + usernameTextField.getHeight() * (float)1.25);
        stage.addActor(usernameTextField);

        final TextField passwordTextField = new TextField("", game.getGameSkin());
        passwordTextField.setMessageText(game.getStrings().get("password"));
        passwordTextField.setPasswordMode(true);
        passwordTextField.setPasswordCharacter('*');
        passwordTextField.setAlignment(Align.center);
        passwordTextField.setWidth(Gdx.graphics.getWidth() / 4);
        passwordTextField.setHeight(50);
        passwordTextField.setPosition((int)(Gdx.graphics.getWidth() / 2 - passwordTextField.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - passwordTextField.getHeight() / 2));
        stage.addActor(passwordTextField);
        //////////

        // Create our login and register buttons
        TextButton loginButton = new TextButton(game.getStrings().get("login"), game.getGameSkin());
        loginButton.setWidth(Gdx.graphics.getWidth() / 4);
        loginButton.setHeight(50);
        loginButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - loginButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - loginButton.getHeight() / 2
                        - passwordTextField.getHeight() * (float)1.5));
        loginButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (usernameTextField.getText().isEmpty() || passwordTextField.getText().isEmpty()) {
                    Dialog dialog = new Dialog(game.getStrings().get("error"), game.getGameSkin()) {
                        public void result(Object obj) {}
                    };

                    dialog.text(game.getStrings().get("login_empty"));
                    dialog.button(game.getStrings().get("ok"), true);
                    dialog.show(stage);
                    return;
                }

                AuthClient client = new AuthClient(usernameTextField.getText(), passwordTextField.getText(),
                        new AuthListener(){
                            @Override
                            public void result(AuthClient c, boolean success) {
                                // Did we authenticate correctly?
                                if (!success) {
                                    Dialog dialog = new Dialog(game.getStrings().get("error"), game.getGameSkin()) {
                                        public void result(Object obj) {}
                                    };

                                    dialog.text(game.getStrings().get("invalid_combo"));
                                    dialog.button(game.getStrings().get("ok"), true);
                                    dialog.show(stage);
                                    return;
                                }

                                // Success, set our username and auth token
                                game.setUsername(c.getUsername());
                                game.setToken(c.getAuthToken());
                            }
                        }, 0);

                Thread clientThread = new Thread(client);
                clientThread.start();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(loginButton);

        TextButton registerButton = new TextButton(game.getStrings().get("register"), game.getGameSkin());
        registerButton.setWidth(Gdx.graphics.getWidth() / 4);
        registerButton.setHeight(50);
        registerButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - registerButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - loginButton.getHeight() / 2
                        - passwordTextField.getHeight() * (float)1.25 * 2));
        registerButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new RegisterScreen());
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(registerButton);
        //////////

        // Create our exit game button
        TextButton exitButton = new TextButton(game.getStrings().get("exit"), game.getGameSkin());
        exitButton.setWidth(Gdx.graphics.getWidth() / 4);
        exitButton.setHeight(50);
        exitButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - exitButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - loginButton.getHeight() / 2
                        - passwordTextField.getHeight() * (float)1.25 * 3));
        exitButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Quit game
                Gdx.app.exit();
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(exitButton);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor((float)0.8, (float)1, (float)1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();

        // Is our login complete?
        if (game.getUsername() != null && game.getToken() != null) {
            // Yes! Go to the menu screen!
            game.setScreen(new MenuScreen());
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
