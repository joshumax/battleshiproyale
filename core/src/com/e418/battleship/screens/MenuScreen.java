package com.e418.battleship.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.e418.battleship.BattleshipRoyale;
import com.e418.battleship.net.GameListInfo;

import java.util.ArrayList;
import java.util.Collections;

public class MenuScreen implements Screen {
    private Stage stage;
    private final BattleshipRoyale game;
    private java.util.List<GameListInfo> gamesList =
            Collections.synchronizedList(new ArrayList<GameListInfo>());

    public MenuScreen() {
        // Get our battleship game instance and create our screen
        game = BattleshipRoyale.getInstance();
        stage = new Stage(new ScreenViewport());

        // Create our Battleship Royale title
        Image title = new Image(game.getGameLogo());
        title.setScale((float)0.5);
        title.setPosition((int)(Gdx.graphics.getWidth() / 2 - title.getWidth() / 4), 0);
        title.setY(Gdx.graphics.getHeight() * 2 / 3);
        stage.addActor(title);

        Label welcome = new Label(game.getStrings().format("welcome", game.getUsername()), game.getGameSkin());
        welcome.setAlignment(Align.center);
        welcome.setY(Gdx.graphics.getHeight() * ((float)2 / 3) - title.getHeight() / 8);
        welcome.setWidth(Gdx.graphics.getWidth());
        stage.addActor(welcome);

        // Create a scroll pane for available games
        Table table = new Table(game.getGameSkin());
        Table container = new Table(game.getGameSkin());
        container.setY(Gdx.graphics.getHeight() * ((float)2 / 3) - title.getHeight() / 8 - 150);
        container.setWidth(Gdx.graphics.getWidth());
        ScrollPane scroll = new ScrollPane(table, game.getGameSkin());
        scroll.setFadeScrollBars(false);
        container.add(scroll).width(Gdx.graphics.getWidth()/4).height(250);
        container.row();
        stage.addActor(container);

        // Server list refresh button
        refreshServerList(table);

        // Create our create game button
        TextButton newGameButton = new TextButton(game.getStrings().get("new_game"), game.getGameSkin());
        newGameButton.setWidth(Gdx.graphics.getWidth() / 4);
        newGameButton.setHeight(50);
        newGameButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - newGameButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - newGameButton.getHeight() / 2
                        - scroll.getHeight() * (float)1.25));
        newGameButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(newGameButton);

        // Create our logout button
        TextButton logoutButton = new TextButton(game.getStrings().get("logout"), game.getGameSkin());
        logoutButton.setWidth(Gdx.graphics.getWidth() / 4);
        logoutButton.setHeight(50);
        logoutButton.setPosition((int)(Gdx.graphics.getWidth() / 2 - logoutButton.getWidth() / 2),
                (int)(Gdx.graphics.getHeight() / 2 - newGameButton.getHeight() / 2
                        - scroll.getHeight() * (float)2));
        logoutButton.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Logout
                game.clearAuthInfo();
                game.setScreen(new LoginScreen());
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(logoutButton);

        // TODO
        for(int i = 0; i < 25; i++) {
            gamesList.add(new GameListInfo("Testing", "fake"));
        }
    }

    private void refreshServerList(final Table table) {
        // Clear out our old table contents
        table.clear();

        // Add our refresh button again
        final float sl_button_width = Gdx.graphics.getWidth()/4 - 25;
        TextButton refresh = new TextButton(game.getStrings().get("refresh_games"), game.getGameSkin());
        refresh.setColor(Color.LIME);

        // Add our refresh event listener
        refresh.addListener(new InputListener(){
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Refresh again
                System.out.println("Refreshing open games list...");
                refreshServerList(table);
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        // Add our button to the table
        table.add(refresh).width(sl_button_width);
        table.row();

        // Get the games from the open game list
        for (GameListInfo info : gamesList) {
            // TODO
            TextButton server = new TextButton(info.getGameName(), game.getGameSkin());
            table.add(server).width(sl_button_width);
            table.row();
        }
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor((float)0.8, (float)1, (float)1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
